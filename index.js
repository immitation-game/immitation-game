var express = require('express');
var mongoose = require('mongoose');
var config = require('./config.json');
var routes  = require("./routes.js");
var swig = require("swig");
var helmet = require("helmet");
var session = require('express-session');
var bodyParser = require('body-parser');

console.log(config);
 var mongoHost = config.db.dev.host;
 var mongoPort = config.db.dev.port;
 var dbConnection = mongoHost + ":" + mongoPort + "/" + config.db.dev.database;

 var app = express();

app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.use(bodyParser.json());
app.use(helmet());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + "/views"));
app.use(session({secret: 'hattori',name: 'diegocookie', secure: true,
    httpOnly: true,resave: true,
    saveUninitialized: true}));

 mongoose.connect(dbConnection);

routes(app);

app.listen(3000);







