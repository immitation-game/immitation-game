(function(){

	var userHandler = require("./handlers/userHandler.js");
	var gameHandler = require("./handlers/gameHandler.js");
	var adminHandler = require("./handlers/adminHandler.js");

	module.exports = function(app){

			app.all("/",userHandler.login);
			app.get("/logout",userHandler.logout);
			app.all("/signup",userHandler.signup);

			app.all("/forgetpassword",userHandler.fpassword);
			app.post("/changepassword",userHandler.changepassword);
			app.get("/profile",userHandler.profile);

			app.get("/play",gameHandler.showQuestion);
			app.post("/submit",gameHandler.submitAns);
			app.get("/leaderboard",gameHandler.leaderboard);
			app.get("/rules",gameHandler.rules);
			app.get("/format",gameHandler.format);

			app.all("/addQuestion",adminHandler.addQuestion);
			app.all("/editQuestion",adminHandler.editQuestion);
			app.get("/reorder",adminHandler.reorder);
			app.get("/removeQuestion",adminHandler.removeQuestion);
			app.all("/dashboard",adminHandler.dashboard);
			app.get("/players",adminHandler.playerinfo)
			app.get("/submissions",adminHandler.submissions);
			app.get("/blockplayer",adminHandler.block);

	};


})();