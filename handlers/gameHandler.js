(function(){


var users = require("../models/User.js");
var questions = require("../models/Question.js");
var answers = require("../models/Answer.js");


function isLoggedIn(req){
	var sess = req.session;
	if(sess.user && sess.user.block)
		return false;
	return sess.user;
}

function GetComparator(key){

	return function(a,b){
		if(a[key] > b[key])
			return 1;
		else
			return -1;
	}
}
function max(a,b)
{
	if(a>b)
		return a;
	else
		return b;
}
function GetComparator(key,key1){

	console.log(key + key1);
	return function(a,b){
		if(a[key] > b[key])
			return -1;
		else if(a[key] < b[key])
			return 1;
		else
		{
			if(a[key1] < b[key1])
				return -1;
			else 
				return 1;
		}
	}
}



module.exports.rules = function(req,res){


	res.render("rules.html");


};

module.exports.format = function(req,res){

	res.render("format.html");
}

module.exports.showQuestion = function(req,res){

	if(!isLoggedIn(req))
	{
		res.redirect("/");
	}
	else
	{
	var level = req.query.level;
	if(!(level && level!=undefined && level>0))
		res.render("error.html",{"message":"invalid arguments"});
	else{
	questions.find({},function(err,data){
		if(err)
			res.render("error.html",{"message":"some internal error"});
		else{
		data.sort(GetComparator("QuestionId"));
		console.log(data);
		if(level > data.length)
		{
			res.render("error.html",{"message":"wait for it"});
		}
		else
			res.render("question.html",{"question":data[level-1],"level":level});
		}
	});
}
}

};

module.exports.submitAns = function(req,res){

	if(!isLoggedIn(req))
		res.redirect("/");
	else
	{

	var ans = req.body.answer;
	//ans = ans.toLowerCase();
	//ans = ans.replace(" ","");
	var qid = req.body.questionId;
	var level = req.body.level;

	if(!(ans && qid && ans != undefined && qid != undefined && level != undefined && level))
		res.render("error.html",{"message":"invalid arguments"});
	else
	{
	answers.findOne({"questionId":qid},function(err,data){
		//console.log(data);
		if(err)
			res.render("error.html",{"message":"some internal error"});
		else if(err || !data)
			res.render("error.html",{"message":"some internal error"});
		else{
	var sess = req.session;
	var curr_user = sess.user;
	var tries = 0;
	var Submissions = curr_user.Submissions;
	curr_user.ltime = (new Date()).getTime();
	for(var i=0;i<Submissions.length;i++)
	{
		if(Submissions[i].questionId == qid)
			tries = Submissions[i].tries;
	}
	var can = false;
	if(tries != 0)
	{
		for(var i=0;i<Submissions.length;i++)
		{
			if(Submissions[i].questionId == qid)
				{
					(Submissions[i].tries)++;
					ans = {"answer":ans};
					Submissions[i].submiss.push(ans);
				}
		}
		can = true;
	}
	else
	{
		var subm = new Array();
		ans = {"answer" : ans};
		subm.push(ans);
		var submission_data = {"questionId": qid,"tries":1,"submiss":subm};
		Submissions.push(submission_data);
		can = true;
	}
	while(!can){};
	can = false;
	var message  = "";
	console.log(data.answer);
	console.log(ans["answer"]);
	if(data.answer == ans["answer"])
	{
		message = "Correct answer";
		leve = Number.parseInt(level);
		(curr_user.level) = max(curr_user.level, leve + 1);
		can = true;
	}
	else
	{
		message  = "Wrong Answer";
		can  = true;
	}
	while(!can){};	
	console.log(curr_user);
	console.log(curr_user.Submissions);
	users.updateOne({"userName":curr_user.userName},curr_user,function(err,data){
		
		if(err)
		{
			console.log(err);
			res.render("error.html",{"message":"some internal error"});
		}
		else
		res.render("result.html",{"message":message});
	});
}
});
}
}
};

module.exports.leaderboard = function(req,res){

	users.find({},function(err,data){
		if(err)
			res.render("error.html",{"message":"some internal error"});
		else
		{
		data = data.sort(GetComparator("level","ltime"));
		data.slice(0,10);
		res.render("leaderboard.html",{"userdata":data});
		}
	});
};


})();