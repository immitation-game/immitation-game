(function(){


var users = require("../models/User.js");
var questions = require("../models/Question.js");
var answers = require("../models/Answer.js");


function adminAuth(req){

	var session = req.session;
	if(!session.user)
		return false;
	else 
	return session.user["Admin"];
}

module.exports.addQuestion = function(req,res){

	if(!adminAuth(req))
		res.render("error.html",{"message":"not authorised"});
	else if(req.method == "GET")
	{
		questions.find({},function(err,data){
			console.log(data);
			if(err && !data)
				res.render("error.html",{"message":"some internal error"});
			else{
			var len = data.length;
			console.log(len);
			var ind = 1;
			if(len > 0)
				ind = data[len-1].questionId + 1;
			console.log(ind);
			res.render("addques.html",{"link":"/addQuestion","buttonName":"add","details":{"qid":ind,"question":"","answer":""}});
		}
		});
	}
	else if(req.method == "POST")
	{
		var qid = req.body.qid;
		var question = req.body.question;
		var answer = req.body.answer;
		if(!(qid && question && answer) || !(qid != undefined && answer != undefined && question != undefined))
		{
			res.render("error.html",{"message":"invalid parameters"});
		}
		else{
		questions.find({"questionId":qid},function(err,data){

				console.log(data);
				if(err)
					res.render("error.html",{"message":"some internal error"});
				else if(data.length > 0)
					res.render("error.html",{"message":"invalid qid"});
				else
				{
				questions.create({"questionId":qid,"question":question},function(err,data){

						if(err)
							res.render("error.html",{"message":"some internal error"});
						else{
					answers.create({"questionId":qid,"answer":answer},function(err,data){
						
						if(err)
							res.render("error.html",{"message":"some internal error"});
						else
							res.redirect("/dashboard");

					});
				}

				});
			}


		});
	}

	}

};


module.exports.dashboard = function(req,res){


	if(!adminAuth(req))
		res.render("error.html",{"message":"not authorised"});
	else
	{
		questions.find({},function(err,data){

		if(err)
			res.render("error.html",{"message":"some internal error"});
		else
		{
		answers.find({},function(err,dat){

			if(err)
				res.render("error.html",{"message":"some internal error"});
			else
			{
			var info = [];
			for(var i=0;i<data.length;i++)
			{
				var da = data[i];
				da["answer"] = dat[i].answer;
				console.log(da);
				info.push(da);
			}
			res.render("admindash.html",{"info":info});
		}
		});
	}

	});

}


};

module.exports.editQuestion = function(req,res){

	if(!adminAuth(req))
		res.render("error.html",{"message":"not authorised"});
	else if(req.method == "GET")
	{
		var qid = req.query.qid;
		if(!qid && qid == undefined)
			res.render("error.html",{"message":"invalid id"});
		else
		{
		questions.find({},function(err,data){
			if(err)
				res.render("error.html",{"message":"some internal error"});
			else
			{
			var len = data.length;
			var ind = 0;
			for(var i=0;i<len;i++)
			{
				if(data[i].questionId == qid)
					ind = i;
			}
			answers.findOne({"questionId":qid},function(err,ans){
				if(err || !(ans))
					res.render("error.html",{"message":"some internal error"});
				else
				res.render("addques.html",{"link":"/editQuestion","buttonName":"update","details":{"qid":qid,"question":data[ind].question,"answer":ans.answer}});
			});
		}
		});
		}
	}
	else if(req.method == "POST")
	{
		var qid = req.body.qid;
		var question = req.body.question;
		var answer = req.body.answer;
		if(!(qid && question && answer) || !(qid != undefined && answer != undefined && question != undefined))
		{
			res.render("error.html",{"message":"invalid parameters"});
		}
		else{
		questions.findOneAndUpdate({"questionId":qid},{"questionId":qid,"question":question} ,function(err,data){

						if(err)
							res.render("error.html",{"message":"some internal error"});
						else{
					answers.findOneAndUpdate({"questionId":qid},{"questionId":qid,"answer":answer},function(err,data){
						
						if(err)
							res.render("error.html",{"message":"some internal error"});
						else
							res.redirect("/dashboard");

					});
				}

		});
	}

	}

};

module.exports.removeQuestion = function(req,res){

	if(!adminAuth(req))
		res.render("error.html",{"message":"not authorised"});
	else
	{
	var qid = req.query.qid;
	if(!qid || qid == undefined)
	{
		res.render("error.html",{"message":"invalid parameters"});
	}
	else
	{
	questions.deleteOne({"questionId":qid},function(err){
		if(err)
			res.render("error.html",{"message":"some internal error"});
		else
		{
			answers.deleteOne({"questionId":qid},function(err){
			if(err)
			res.render("error.html",{"message":"some internal error"});		
			res.redirect("/dashboard");
		});
	}
	});	
}

}

};


module.exports.playerinfo = function(req,res){

	if(!adminAuth(req))
		res.render("error.html",{"message":"not authorised"});
	else
	{
	users.find({},function(err,data){

		if(err)
			res.render("error.html",{"message":"some internal error"});
		else
			res.render("players.html",{"players":data});
	});
}

};

module.exports.submissions = function(req,res)
{
	if(!adminAuth(req))
		res.render("error.html",{"message":"not authorised"});
	else
	{
	var uname = req.query.uname;
	if(!(uname && uname != undefined))
		res.render("error.html",{"message":"invalid parameters"});
	else
	{
	users.find({"userName":uname},function(err,data){
		if(err)
			res.render("error.html",{"message":"some internal error"});	
		else
		{
			var datas = JSON.stringify(data,null,"\n");
			datas = datas.split("\n");
		res.render("playerinfo.html",{"uname":uname,"datas":datas});
	}
	});
}
}
};

module.exports.block = function(req,res){

if(!adminAuth(req))
		res.render("error.html",{"message":"not authorised"});
	else{

	var uname = req.query.uname;
	if(!(uname && uname != undefined))
		res.render("error.html",{"message":"invalid parameters"});
	else
	{
	users.find({"userName":uname},function(err,data){
		if(err)
			res.render("error.html",{"message":"some internal error"});	
		else
		{
		data.block = true;
		users.updateOne({"userName":uname},data,function(err,data){
			if(err)
				res.render("error.html",{"message":"some internal error"});
			res.redirect("/dashboard");
		});
	}
	});
}
}
};

module.exports.reorder = function(req,res){

if(!adminAuth(req))
		res.render("error.html",{"message":"not authorised"});
else
{
	var qid1 = req.query.qid1;
	var qid2 = req.query.qid2;

	questions.findOne({"questionId":qid1},function(err,data){
		console.log(data);
		if(err)
			res.render("error.html",{"message":"some internal error"});
		else
		{	
		questions.findOne({"questionId":qid2},function(err,data2){

			if(err)
			res.render("error.html",{"message":"some internal error"});
			else
			{
			data.questionId = qid2;
			data2.questionId = qid1;

			questions.deleteOne({"questionId":qid1},function(err,data){
				if(err)
			res.render("error.html",{"message":"some internal error"});
				else
				{
				questions.deleteOne({"questionId":qid2},function(err,data){
					if(err)
			res.render("error.html",{"message":"some internal error"});
			else{
					questions.create(data,function(err,data){
						if(err)
			res.render("error.html",{"message":"some internal error"});
						else
						{
						questions.create(data2,function(err,data){
							if(err)
			res.render("error.html",{"message":"some internal error"});
							else
							{
								console.log("success");
								res.redirect("/dashboard");
							}


						});
					}
					});
				}
				});
			}
			
			});

		}
		});
	}

	});
}

};


})();

