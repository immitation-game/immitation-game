(function(){


var users = require("../models/User.js");
var questions = require("../models/User.js");
var answers = require("../models/User.js");
var bcrypt = require('bcryptjs');
const no_of_rounds = 4; 

function isLoggedIn(req){
	var sess = req.session;
	if(sess.user  && sess.user.block)
		return false;
	return sess.user;
}

function Hash(pass){
	var salt = bcrypt.genSaltSync(no_of_rounds);
	var hash = bcrypt.hashSync(pass,salt);
	return hash;
}


module.exports.login = function(req,res){

							if(req.method == "GET")
							{
								if(isLoggedIn(req))
								{
									var sess = req.session;
									var level = sess.user["level"]
									level = encodeURIComponent(level);
									res.redirect("/play?level=" + level);
								}
								else
									res.render("login.html");
							}

							if(req.method == "POST")
							{
								var uname = req.body.uname;
								var passwd = req.body.passwd;
								//console.log("test:" + uname);
								if(!(uname && passwd && uname != "" && passwd != "" && uname != undefined && passwd != undefined))
								{
									res.render("error.html",{"message":"Some error with your request"});
								}
								else
								{
								users.findOne({"userName":uname},function(err,data){

									console.log(data);
									if(err)
										res.render("error.html",{"message":"some internal error"});

									if(data == null || data == undefined)
									{
										res.render("login.html",{"message":"invalid username"});
									}
									else if(bcrypt.compareSync(passwd,data.password))
									{
												sess = req.session;
												sess.user = data;
												res.redirect('/');
									}
									else
										res.render("login.html",{"message":"wrong password"});
								});
							}
							}




};


module.exports.signup = function(req,res){


		if(req.method == "GET")
		{
			if(isLoggedIn(req))
			{
				var sess = req.session;
				var level = sess.user["level"]
				level = encodeURIComponent(level);
				res.redirect("/play?level=" + level);
			}
			else
				res.render("signup.html");
		}
		if(req.method == "POST")
		{
			var uname = req.body.username;
			var pass = req.body.password;
			var cpass = req.body.cpass;
			var name = req.body.name;
			var email = req.body.email;
			var phone = req.body.phone;
			var dept = req.body.dept;
			var year = req.body.year;
			var college = req.body.college;			

			if(!(uname && pass && cpass && name && email && phone && uname!="" && cpass !="" && pass !="" && name !="" && phone !="" && email !="" && dept && year && college) || pass != cpass)
			{
				res.render("error.html",{"message":"Invalid arguments"});
			}
			else if(uname == undefined || cpass == undefined || pass == undefined || name == undefined || phone == undefined || email == undefined || dept == undefined || year == undefined || college == undefined)
				res.render("error.html",{"message":"Invalid arguments"});
			else{
			users.findOne({"userName":uname},function(err,data){

					if(err)
						res.render("error.html",{"message":"some internal error"});

					else if(data)
						res.render("signup.html",{"message":"username already taken"});
					else
					{
					users.findOne({"email":email},function(err,dat){

						if(dat)
							res.render("signup.html",{"message":"username already taken"});
						else
						{
						var hashpass = Hash(pass);
						var nus = {"name":name,"password":hashpass,"userName":uname,"level":1};
						nus["phone"] = phone;
						nus["email"] = email;
						nus["Admin"] = false;
						nus["ltime"] = 0;
						nus["dept"] = dept;
						nus["year"] = year;
						nus["college"] = college;
						nus["Submissions"] = new Array();
						nus["block"] = false;
						users.create(nus,function(err,data2){
							if(err)
								res.render("error.html",{"message":"some internal error"});
							else
							res.redirect("/");
						});
					}
					});	
					}				


			});

		}
	}

};

module.exports.profile = function(req,res){

	if(!isLoggedIn(req))
		res.render("error.html",{"message":"you must be logged in"});
	else
	{
		var curr_user = req.session.user;
		res.render("profile.html",{"user":curr_user});
	}
};
module.exports.fpassword = function(req,res){

	if(req.method == "GET")
	{
		res.render("fpass.html");
	}
	else if(req.method == "POST")
	{
		var email = req.body.email;
		if(!email || email == undefined)
			res.render("error.html",{"message":"some internal error"});
		users.findOne({"email":email},function(err,data){
			if(err)
				res.render("error.html",{"message":"some internal error"});
			else if(!data)
				res.render("error.html",{"message":"no such email"});
			else
			{
			var npass = Math.floor((Math.random()*1000000)) + "";
			data.password = hash(npass);
			users.updateOne({"userName":data.userName},data,function(err,result){
				sendMsg(data.phone,npass);
				res.redirect("/");
		});
		}});
	}

};

module.exports.changepassword = function(req,res){

	if(!isLoggedIn(req))
		res.redirect("/");	
	else
	{
	var opass = req.body.oldpassword;
	var npass = req.body.newpassword;
	var cpass = req.body.confirmpassword;
	var session = req.session;
	if(!(opass && npass && cpass)|| opass == undefined || npass == undefined || cpass == undefined)
		res.render("error.html",{"message":"Invalid arguments"});
	else if(npass != cpass)
		res.render("error.html",{"message":"New password and confirm password doesn't match"});
	else if(bcrypt.compareSync(opass,session.user.password))
	{
		session.user.password = Hash(npass);
		users.updateOne({"uname":session.user.uname},session.user,function(err,data){
				if(err)
					res.render("error.html",{"message":"some internal error"});
				else
				res.redirect("/");
		});
	}
	else
	res.render("error.html",{"message":"Incorrect password"});	
}


};

module.exports.logout = function(req,res){

	var sess  = req.session;
	sess.user = null;
	res.redirect("/");
}

})();