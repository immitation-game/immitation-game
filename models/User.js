var mongoose = require('mongoose');

var users = mongoose.Schema({

    userName:String,
    password:String,
    name: String,
    email: String,
    level: String,
    phone: Number,
    Admin: Boolean,
    ltime: Number,
    block: Boolean,
    College: String,
    Dept: String,
    Year: Number,
    Submissions: [
    	{
    		questionId:Number,
    		tries:Number,
    		submiss:[{
    			answer: String
    		}
    		]
    	}
    ]
    
},  {
        collection:"Users"
});

module.exports = mongoose.model('Users',users);