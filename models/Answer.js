var mongoose = require('mongoose');

var answers = mongoose.Schema({

	questionId : Number,
	answer : String

},
{
	collection:"Answers"
});

module.exports = mongoose.model('Answers',answers);