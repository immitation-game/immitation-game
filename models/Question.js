var mongoose = require('mongoose');

var questions = mongoose.Schema({

	questionId : Number,
	question : String

},
{
	collection:"Questions"
});

module.exports = mongoose.model('Questions',questions);